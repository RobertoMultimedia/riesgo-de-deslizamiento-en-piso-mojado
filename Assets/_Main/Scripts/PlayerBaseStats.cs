﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using KartGame.KartSystems;


public class PlayerBaseStats : MonoBehaviour
{
    [Header("Stats"), SerializeField]
    private float topSpeed;

    [SerializeField]
    private float acceleration;

    [SerializeField]
    private float reverseSpeed;

    [SerializeField]
    private float reverseAceleration;

    [SerializeField, Range(0.2f, 1)]
    private float accelerationCurve;

    [SerializeField]
    private float braking;

    [SerializeField]
    private float coastingDrag;

    [SerializeField, Range(0, 1)]
    private float grip;

    [SerializeField]
    private float steer;

    [SerializeField]
    private float addedGravity;

    [SerializeField, Range(0, 1)]
    private float suspension;

    [SerializeField, Header("")]
    private ArcadeKart player;

    private ArcadeKart.Stats baseStats;


    [Header("Events")]
    public UnityEvent CrashMessage;
    public UnityEvent WetCrashMessage;
    public UnityEvent PlayerOutControlEvent;
    public UnityEvent PlayerTakeControlEvent;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void StatsContructor(float topSpeed, float acceleration, float reverseSpeed, float reverseAceleration, float accelerationCurve, float braking, float coastingDrag, float grip, float steer, float addedGravity, float suspension)
    {
        player.baseStats.TopSpeed = topSpeed;
        player.baseStats.Acceleration = acceleration;
        player.baseStats.ReverseSpeed = reverseSpeed;
        player.baseStats.ReverseAcceleration = reverseAceleration;
        player.baseStats.AccelerationCurve = accelerationCurve;
        player.baseStats.Braking = braking;
        player.baseStats.CoastingDrag = coastingDrag;
        player.baseStats.Grip = grip;
        player.baseStats.Steer = steer;
        player.baseStats.AddedGravity = addedGravity;
        player.baseStats.Suspension = suspension;
    }
    public void PlayerOutControlStats()
    {
        if (player.currentSpeed >= 40)
        {
            StatsContructor(topSpeed, acceleration, reverseSpeed, reverseAceleration, accelerationCurve, braking, coastingDrag, grip, steer, addedGravity, suspension);
            PlayerOutControlEvent.Invoke();
        }
        else
        {
            StatsContructor(50f, 5f, 5f, 5f, 0.8f, 10f, 4f, 0.95f, 5f, 1f, 0.2f);
            PlayerTakeControlEvent.Invoke();
        }
    }

    

    public void PlayerStandarBaseStats()
    {
        StatsContructor(50f, 5f, 5f, 5f, 0.8f, 10f, 4f, 0.95f, 5f, 1f, 0.2f );

    }

    public void PlayerMessages()
    {
        if (player.currentSpeed >= 40)
        {
            WetCrashMessage.Invoke();
        }
    }

}
