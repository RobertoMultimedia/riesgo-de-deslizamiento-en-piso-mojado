﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using KartGame.KartSystems;

public class GameManager : MonoBehaviour
{
    [Header("UI"), SerializeField]
    private Text speedText;
    [SerializeField]
    private ArcadeKart arcadeKart;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        speedText.text = Mathf.Round(arcadeKart.currentSpeed).ToString();
    }
}
