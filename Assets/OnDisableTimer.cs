﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnDisableTimer : MonoBehaviour
{
    [SerializeField]
    private float timer;

    IEnumerator OnDisableCorrutine()
    {
        yield return new WaitForSeconds(timer);
        gameObject.SetActive(false);
    }
    public void OnDisableObjec()
    {
        StartCoroutine(OnDisableCorrutine());
    }
}
